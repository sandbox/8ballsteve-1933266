<?php

/**
 * Return an array of imagestyles
 */
function _field_file_mover_image_styles() {
  $styles = array();
  foreach (image_styles() as $id => $style) {
    $styles[] = $id;
  }
  
  return drupal_map_assoc($styles);
}

/**
 * Return any settings
 * - cached for performance
 * @todo: configurable supported widgets
 */
function _field_file_mover_get_settings() {
  
  $settings = &drupal_static(__FUNCTION__);
  if (!isset($settings)) {
    
    if ($cache = cache_get('field_file_mover_settings')) {
      $settings = $cache->data;
    }
    else {
  
      $bundles = field_info_instances('node');
      foreach ($bundles as $bundles => $fields) {
        foreach ($fields as $key => $field) {
          if ($field['widget']['type'] == 'image_image' && ($field['widget']['settings']['field_file_mover_enable'] != 0)) {
            $settings[$key] = $field['widget']['settings']['field_file_mover_styles_list'];
          }
        }
      }      
      cache_set('field_file_mover_settings', $settings, 'cache');
    }
  }
  
  return $settings;
}

/**
 * Retrieve required styles for a field
 */
function _field_file_mover_field_settings($field) {
  $settings = array();  
  if ($all_settings = _field_file_mover_get_settings()) {
    if ($field_settings = $all_settings[$field]) {
    
      foreach ($field_settings as $style) {
        if ($style):  
          $settings[$style] = $style;
        endif;        
      }
    }
  }
  return $settings;
}
