<?php

/**
 * Implements hook_form_FORM_ID_alter()
 * @todo: add support for further widgets
 */
function field_file_mover_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  // Get the instance and widget type.
  $element = &$form['instance'];
  $widget_type = $element['widget']['type']['#value'];
  
  if ($widget_type == 'image_image') {
    // Get the field instance and setting.
    $instance = field_info_instance($element['entity_type']['#value'], $element['field_name']['#value'], $element['bundle']['#value']);
    $settings = $instance['widget']['settings'];
    
    // add our fieldset to the field form
    $element['settings'] += field_file_mover_field_settings_form($settings);
    // add our custom submit callback
    $form['#submit'][] = 'field_file_mover_form_submit';
  }
}

function field_file_mover_field_settings_form($settings) {
  $form = array();
  
  // Add the file_mover fieldset.
  $form['field_file_mover'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field File Mover'),
    '#description' => t(''),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 25,
    '#parents' => array('instance', 'widget', 'settings'),
  );

  $form['field_file_mover']['field_file_mover_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#description' => t('Enable File mover for this field.'),
    '#default_value' => ($settings['field_file_mover_enable']),
  );
  
  $style_options = _file_mover_image_styles();
  
  $form['field_file_mover']['file_mover_styles_list'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Required styles'),
    '#description' => t('Select all styles that are required before this file can be moved.'),
    '#options' => $style_options,
    '#default_value' => isset($settings['field_file_mover_styles_list']) ? $settings['field_file_mover_styles_list'] : array(),
    '#multicolumn' => array('width' => 3),
  );
  
  return $form;
}

/**
 * Save file_mover settings
 */
function field_file_mover_form_submit($form, &$form_state) {
  cache_clear_all('field_file_mover_settings', 'cache', TRUE); 
}
